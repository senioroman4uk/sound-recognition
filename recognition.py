from Queue import Queue

import numpy as np
import sounddevice as sd
from pymongo import MongoClient

from config import LOWER_LIMIT, UPPER_LIMIT
from hashing import get_hash, get_index
from sound_math import fft

fs = 44100
sd.default.blocksize = 1024
sd.default.samplerate = fs
sd.default.channels = 1
duration = 30 # seconds


mongo_client = MongoClient()
db = mongo_client['audio']
match_map = {}


def determine_key_points(results):
    highscores = np.zeros([len(results), 5])
    record_points = np.zeros([len(results), UPPER_LIMIT])
    points = np.zeros([len(results), 5], dtype='int64')

    for t in range(0, len(results), 1):
        for freq in range(LOWER_LIMIT, UPPER_LIMIT - 1, 1):
            mag = np.log10(np.absolute(results[t][freq]) + 1)
            index = get_index(freq)

            if mag > highscores[t][index]:
                highscores[t][index] = mag
                record_points[t][freq] = 1
                points[t][index] = freq

        h = get_hash(points[t][0], points[t][1], points[t][2], points[t][3])
        record = db.aggregated.find_one({'_id': h})
        if not record:
            continue

        for song in record['songs']:
            offset = abs(song['time'] - t)
            tmp_map = match_map.get(song['songid'], None)
            if not tmp_map:
                tmp_map = {offset: 1}
                match_map[song['songid']] = tmp_map
            else:
                count = tmp_map.get(offset, 0)
                tmp_map[offset] = count + 1

    songs = db['songs'].find({})
    bestCount = 0
    bestSong = None

    for s in songs:
        best_count_for_song = 0
        tmp_map = match_map.get(s['name'])
        if not tmp_map:
            continue

        for (key, val) in tmp_map.iteritems():
            if val > best_count_for_song:
                best_count_for_song = val

        if best_count_for_song > bestCount:
            bestCount = best_count_for_song
            bestSong = s

    print "best song", bestSong['name']


q = Queue()

myrecording = sd.rec(int(duration * fs), blocking=True)


results = fft(myrecording)
determine_key_points(results)
