import argparse
import os

import time
import progressbar
from pydub import AudioSegment
from pymongo import MongoClient

from hashing import get_hash
from sound_math import fft, determine_key_points

db = {}
mongo_client = MongoClient()
audioDb = mongo_client['audio']

parser = argparse.ArgumentParser()
parser.add_argument('--filename', help='audio file to index')
parser.add_argument('--directory', help='directory with songs to process')

args = parser.parse_args()
if not args.filename and not args.directory:
    parser.exit(-1, 'filename or directory should be specified')


def process_file(path_to_file):
    song = AudioSegment.from_file(path_to_file).set_channels(1).set_frame_rate(44100).set_sample_width(1)
    data = song.get_array_of_samples()
    results = fft(data)
    key_points = determine_key_points(results)
    for t in range(0, len(results), 1):
        h = get_hash(key_points[t][0], key_points[t][1], key_points[t][2], key_points[t][3])
        pts = db.get(h, [])
        pts.append({'songid': path_to_file, 'time': t})
        db[h] = pts


try:
    files_to_process = []
    if args.filename:
        files_to_process.append(args.filename)
    else:
        for filename in os.listdir(args.directory):
            if not filename.endswith('.mp3'):
                continue

            files_to_process.append(filename)

    songs = []
    progress_bar = progressbar.ProgressBar()

    for f in progress_bar(files_to_process):
        file_name = os.path.join(args.directory, f)
        process_file(file_name)
        songs.append({'name': file_name})

    records = []
    for key in db:
        records.append({'hash': key, 'songs': db[key]})

    audioDb['marks'].insert(records)
    audioDb['songs'].insert(songs)

except KeyboardInterrupt:
    exit(0)
