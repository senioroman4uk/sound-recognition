from config import DEFAULT_FUZ_FACTOR, RANGE


def get_hash(p1, p2, p3, p4):
    return str((p4 - (p4 % DEFAULT_FUZ_FACTOR)) * 100000000 + (p3 - (p3 % DEFAULT_FUZ_FACTOR)) * 100000 +
               (p2 - (p2 % DEFAULT_FUZ_FACTOR)) * 100 + (p1 - (p1 % DEFAULT_FUZ_FACTOR)))


def get_index(freq):
    i = 0
    while RANGE[i] < freq:
        i = 1 + i
    return i
