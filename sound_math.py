import numpy as np
from config import DEFAULT_CHUNK_SIZE, LOWER_LIMIT, UPPER_LIMIT
from hashing import get_index


def fft(audio, chunk_size=DEFAULT_CHUNK_SIZE):
    total_size = len(audio)
    amount_possible = total_size / chunk_size
    results = []
    for times in range(0, amount_possible, 1):
        complex = np.empty(chunk_size, dtype='complex')
        for i in range(0, chunk_size, 1):
            complex[i] = np.complex(audio[(times * chunk_size) + i], 0.0)

        results.append(np.fft.fft(complex))

    return results


def determine_key_points(results):
    highscores = np.zeros([len(results), 5])
    points = np.zeros([len(results), 5], dtype='int64')

    for t in range(0, len(results), 1):
        for freq in range(LOWER_LIMIT, UPPER_LIMIT - 1, 1):
            mag = np.log10(np.absolute(results[t][freq]) + 1)
            index = get_index(freq)

            if mag > highscores[t][index]:
                highscores[t][index] = mag
                points[t][index] = freq

    return points

